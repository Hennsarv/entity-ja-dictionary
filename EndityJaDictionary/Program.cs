﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EndityJaDictionary
{
    partial class Aine
    {
        public override string ToString()
        {
            return $"{AineKood}: {Nimetus} ({maht})";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            ManguKoolEntities me = new ManguKoolEntities();

            me.Ained.ToList().ForEach(x => me.Ained.Remove(x));

            me.Ained.Add(new Aine() { Nimetus = "Hüppamine", AineKood = "hypex" });
            me.Ained.Add(new Aine() { Nimetus = "Kargamine", AineKood = "kargx" });
            me.Ained.Add(new Aine() { Nimetus = "Jooksmine", AineKood = "jookx" });

            me.SaveChanges();


            foreach (var x in me.Ained)
                Console.WriteLine(x);

            
            var dict = me.Ained.ToDictionary(x => x.AineKood, x => x);

            Console.Write("mis ainet sa tahad: ");
            string kood = Console.ReadLine();
            Console.WriteLine(dict[kood]);

            // ära taha dictionary

            Console.WriteLine(me.Ained.Where(x => x.AineKood == kood).SingleOrDefault());

            // näiteks tahad sa muuta ainemahtu

            dict[kood].maht = 40; me.SaveChanges();
            Console.WriteLine(me.Ained.Where(x => x.AineKood == kood).SingleOrDefault());


        }
    }
}
